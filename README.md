# FaceTrack controller

This is the design idea/concept for the Capstone case in the CPP Embedded Development Bootcamp. As an alternative to the project, I was asked to present a sales pitch, and the design for my project.

The FaceTrack controller would be an AI-enabled controller script utilizing DNN to remotely control peripherals on another platform.

## Table of Contents

 - [Requirements](#requirements)
 - [Usage](#usage)
 - [Maintainers](#maintainers)
 - [Contributing](#contributing)
 - [License](#license)

## Requirements

For **facetrack.py**, it is required to install the following python libraries: `opencv2`, `mediapipe`, `vgamepad`, `numpy`.

The powerpoint slides, and the design/pseudocode of the presentation are also included.

A link to the **sales pitch** presentation is provided separately.

## Usage

You can use your python interpreter of choise to test the script.

## Maintainers

[Elmo Rohula @rootElmo](https://gitlab.com/rootElmo)

## Contributing

## License