"""
===== SENDER LOGIC pseudocode =====
# imports
import cv2 as cv
import pytorch
import numpy as np
from server import server
...

# Init camera

cap = cv.VideoCapture(0)
if not cap.isOpened():
    print("Can't access camera")
    quit

# Init model, and load trained model
model = Model()
model.load_state_dict(torch.load('model-path/model'))
model.eval()

# Init server
server = Server(addr, port)

# Init receiver
receiver = Receiver(addr, port)

# Resize dimensions for captured image
# for NN's prediction to work
dim = (width_for_ai, height_for_ai)

def img_to_data(image):
    # Turning captured frame into
    # data that can be evaluated by the NN
    return data

def send_data(predict):
    server.send(predict, addr:port)

def data_to_img(img):
    # Turn received data to usable image data
    return ret_img

# Read camera input
while True:
    ret, frame = cap.read()
    frame = cv.2resize(frame, dim, interpolation)

    with torch.no_grad():

        # Make a prediction and send data over IP
        frame = img_to_data(frame)
        prediction = model(frame)
        predicted_class = np.argmax(prediction)
        send_data(predicted_class)
    if receiver.read():
        # Receive and display image
        img = data_to_img(receiver.data())
        cv.imshow('feed', img)

# Release camera resources
cv.destroyAllWindows()
cap.release()
"""