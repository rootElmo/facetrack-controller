"""
A small script for emulating controller movement by
moving your head. Can be used with any game that supports
XBox360 or PS4 dualshock controllers.

Especially useful in racing games (Assetto Corsa, Dirt Rally etc.),
flight simulators (DCS), or games which support separate head control
(Arma3, DayZ)

Author: Elmo Rohula
"""

import cv2
import mediapipe as mp
import vgamepad as vg
import numpy as np
import math
from enum import Enum

mp_face_detection = mp.solutions.face_detection
mp_drawing = mp.solutions.drawing_utils

# points = np.array(["LEFT_EYE", "RIGHT_EYE", "NOSE_TIP"])
track_points_amnt = 3
track_points = np.empty([track_points_amnt, 2])
track_box = np.empty([4])

smoothing_points = 5

jstick_inter_x = np.empty([smoothing_points], dtype=int)
jstick_inter_y = np.empty([smoothing_points], dtype=int)

gamepad = vg.VX360Gamepad()
x_max = 32768
x_max_range = 65538

deadzone = 0.2
deadzone_y = 0.1
ctrl_x_mid = 0.5

draw_mp = False

class Points(Enum):
    L_EYE = 1
    R_EYE = 0
    NOSE = 2

class Box(Enum):
    START_X = 0
    START_Y = 1
    W = 2
    H = 3

def get_face_track(lmarks):
    for i in range(track_points_amnt):
        dot_x = lmarks[0].location_data.relative_keypoints[i].x
        dot_y = lmarks[0].location_data.relative_keypoints[i].y
        track_points[i] = (dot_x, dot_y)
    for dots in track_points:
        cv2.circle(image, (int(dots[0] * image.shape[1]), int(dots[1] * image.shape[0])), 2, (0, 255, 127), -1)

def draw_face_box(lmarks):
    rec_min_x = lmarks[0].location_data.relative_bounding_box.xmin
    rec_min_y = lmarks[0].location_data.relative_bounding_box.ymin
    rec_max_x = lmarks[0].location_data.relative_bounding_box.width
    rec_max_y = lmarks[0].location_data.relative_bounding_box.height

    np.put(track_box, [0,1,2,3], [rec_min_x, rec_min_y, rec_max_x, rec_max_y])

    # Yoooo snake print function. Longe boi
    cv2.rectangle(image,
                (int(rec_min_x * image.shape[1]), int(rec_min_y * image.shape[0])), # upper left corner
                (int((rec_min_x * image.shape[1]) + (rec_max_x * image.shape[1])), int((rec_min_y * image.shape[0]) + (rec_max_y * image.shape[0]))), # lower right corner
                (255, 255, 255),
                1
                )

# Deprecated. Probably of some use idk.
def get_nose_to_left_angle():
    tri_adj = track_points[Points.NOSE.value][1] - track_box[Box.START_Y.value]
    tri_opp = track_points[Points.NOSE.value][0] - track_box[Box.START_X.value]
    print(f'Tracking angle: 'f'{math.atan(tri_opp / tri_adj)}')

# Get value from 0.0 to 1.0 of nose x, relative to bounding box x
def get_relative_b_box_x(pr_pos=True):
    pos = ((track_points[Points.NOSE.value][0] - track_box[Box.START_X.value])) / track_box[Box.W.value]
    if pr_pos:
        print(f'Nose X relative to Bounding Box X: 'f'{pos}')
    return pos

# Get value from 0.0 to 1.0 of nose y, relative to bounding box y
def get_relative_b_box_y(pr_pos=True):
    pos = ((track_points[Points.NOSE.value][1] - track_box[Box.START_Y.value])) / track_box[Box.H.value]
    if pr_pos:
        print(f'Nose Y relative to Bounding Box X: 'f'{pos}')
    return pos

def get_ctrl_x_pos(x_factor):
    if x_factor > (ctrl_x_mid + deadzone):
        return int(x_max_range * 0.5)
    elif x_factor < (ctrl_x_mid - deadzone):
        return int(x_max_range * 0.5) * -1
    return x_max - int(x_max_range * 0.5)

def get_ctrl_y_pos(y_factor):
    if y_factor > (ctrl_x_mid + deadzone_y):
        return int((x_max_range * 0.5) * 0.35) * -1
    elif y_factor < (ctrl_x_mid - deadzone_y):
        return int(x_max_range * 0.5) * 0.35
    return x_max - int(x_max_range * 0.5)

# For webcam input:
cap = cv2.VideoCapture(0)
with mp_face_detection.FaceDetection(
        model_selection=0, min_detection_confidence=0.5) as face_detection:
    while cap.isOpened():
        success, image = cap.read()
        if not success:
            print("Ignoring empty camera frame.")
            # If loading a video, use 'break' instead of 'continue'.
            continue

        # To improve performance, optionally mark the image as not writeable to
        # pass by reference.
        image.flags.writeable = False
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        results = face_detection.process(image)

        # Draw the face detection annotations on the image.
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        nose_to_left_angle = 0

        ctrl_left_j = x_max_range - x_max

        if results.detections:
            get_face_track(results.detections)
            draw_face_box(results.detections)
            # get_nose_to_left_angle()
            x_fac = get_relative_b_box_x(False)
            y_fac = get_relative_b_box_y(False)

            ctrl_left_j_x = get_ctrl_x_pos(x_fac)
            ctrl_left_j_y = get_ctrl_y_pos(y_fac)

            gamepad.left_joystick(x_value=int(ctrl_left_j_x), y_value=int(ctrl_left_j_y))

            if draw_mp:
                for detection in results.detections:
                    mp_drawing.draw_detection(image, detection)
                
        # Flip the image horizontally for a selfie-view display.
        gamepad.update()
        image = cv2.flip(image, 1)
        cv2.putText(image, str(int(track_points[0][0] * image.shape[1])), (5,15), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 127), 1)
        cv2.putText(image, str(int(track_points[0][1] * image.shape[0])), (5,30), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 255, 127), 1)
        cv2.imshow('MediaPipe Face Detection', image)
        if cv2.waitKey(5) & 0xFF == 27:
            break
cap.release()
