"""
===== RECEIVER LOGIC pseudocode =====
# imports
from camera_pkg import gimbal_camera
from server import receiver
from server import server
...

# Serial comm pins
MISO = 11
MOSI = 12
SCK = 8

# init receiver & camera
receiver = Receiver(addr, port)
server = Server(addr, port)

camera = gimbal_camera(MISO, MOSI, SCK)

def calc_rot_data(rot):
    # Do calculations based on received data
    return rot_amt

# "main loop"
while True:
    if receiver.read():
        rot_data = receiver.data()
        camera.rotate(calc_rot_data(rot_data))
    img = camera.read_img(RGB8)
    server.send(img, addr:port)

"""